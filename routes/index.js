
const Router = require('express');
const router = new Router();
const authMiddleware = require('../middleware/authMiddleware');

const authRoutes = require('./auth.routes');
const userRoutes = require('./user.routes');
const noteRoutes = require('./note.routes');

router.use('/auth', authRoutes);
router.use('/users', authMiddleware, userRoutes);
router.use('/notes', authMiddleware, noteRoutes);

module.exports = router;