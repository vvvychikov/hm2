const User = require('../models/User');
const Note = require('../models/Note');
const bcrypt = require('bcryptjs');
const ApiError = require('../error/ApiError');

class UserController {
    async getUser(req, res, next) {
        try {
            const user = await User.findOne({_id: req.user.id});

            if (!user) {
                return next(ApiError.unauthorized('Unauthorized'));
            }

            return res.status(200).json({
                "user": {
                    id: user.id,
                    username: user.username,
                    createdAt: user.createdAt
                }
            })

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async editUser(req, res, next) {
        try {
            const user = await User.findOne({_id: req.user.id});

            if (!user) {
                return next(ApiError.unauthorized('Unauthorized'));
            }

            const {oldPassword, newPassword} = req.body;

            const isPasswordCorrect = await bcrypt.compare(oldPassword, user.password);

            if (!isPasswordCorrect) {
                return next(ApiError.badRequest('Password is incorrect'));
            }

            const hashedPassword = await bcrypt.hash(newPassword, 10);

            await User.findByIdAndUpdate(user.id, {password: hashedPassword});

            return res.status(200).json({message: 'Success'});

        } catch (err) {
            return next(ApiError.internalError('Server error'))
        }
    }

    async deleteUser(req, res, next) {
        try {
            const user = await User.findOne({_id: req.user.id});

            if (!user) {
                return next(ApiError.unauthorized('Unauthorized'));
            }

            await Note.find({userId: user.id}).remove();
            await User.findOneAndDelete({_id: user.id});

            return res.status(200).json({message: "success"});

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }
}

module.exports = new UserController();